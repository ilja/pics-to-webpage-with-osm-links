#!/bin/bash

##
# VARIABLES
##
# The mapcomplete maps to link to
maptypes='artwork benches cafes_and_pubs maps shops'

##
# SCRIPT
##
echo '<html><head></head><body>' > index.html

function add_to_index() {
  pic="$1"

  # Get location data
  latidtude=$(exiftool -b -s3 -GPSLatitude ./pics/$pic)
  longitude=$(exiftool -b -s3 -GPSLongitude ./pics/$pic)
  position=$(exiftool -b -s3 -GPSPosition ./pics/$pic)

  echo "<p>" >> index.html
  # Add the preview
  echo "<a target='_blank' href='./pics/$pic'>" >> index.html
  echo "<img src='./pics/$pic' height='120px'/>" >> index.html
  echo "</a><br>" >> index.html
  # Add the osm link
  echo "<a target='_blank' href='https://www.openstreetmap.org/search?whereami=1&query=$latidtude%2C$longitude#map=17/$latidtude/$longitude'>osm: $position</a><br>" >> index.html
  # Add the mapcomplete links
  maptype=artwork
  for maptype in $maptypes
  do
    echo "<a target='_blank' href='https://mapcomplete.osm.be/$maptype.html?z=17&lat=$latidtude&lon=$longitude&language=en#welcome'>mapcomplete, $maptype: $position</a><br>" >> index.html
  done

  echo "</p>" >> index.html
}

for pic in $(ls ./pics)
do
  if [[ "$(exiftool -b -s3 -MIMEType ./pics/$pic)" == image* ]]
  then
    echo "Adding ./pics/$pic..."
    add_to_index "$pic"
  else
    echo "./pics/$pic is not an image, skipping"
  fi
done

echo '</body>' >> index.html
