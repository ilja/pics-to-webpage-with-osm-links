# Pics-to-webpage-with-osm-links
## What

This is a very simple script to take some pics, extract the location data, and provide an index.html which lists the pictures and links to [OpenStreetmap](https://www.openstreetmap.org) and [MapComplete](https://mapcomplete.osm.be).

The idea is that you can go out and take pictures of things you want to map (make sure you have location on and that location is stored in the EXIF data of the pictures). Then you can put these pictures in the `pics` folder of this repository and run the `./to_index.sh` script. It will output an index.html file which you can open in the browser. The file shows the pictures and links to the location on osm and mapcomplete for several maps.

## How to use

It requires `bash` and `exiftool`, so make sure you have these installed.

Place pics (preferably with location data) in the pics folder and run the script `./to_index.sh`.

## Known issues

* When I try with a picture I took (using a BQ4.5 Ubuntu Edition running Ubutnu Touch), I get a wrong location. It's about 20km off. When taking the picture, I was also running Puremaps (a map/routeplanner app) and this one did show me a correct location. I'm unsure atm if the problem is with UT in general, or with the camera app, or with EXIF, or with OSM, or...

## License

    Create a simple webpage from pictures. It links to to OpenStreetMap and
    MapComplete using their location data for easy mapping.
    Copyright (C) 2023  ilja@ilja.space

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
